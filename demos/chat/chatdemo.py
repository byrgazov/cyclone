#!/usr/bin/env python3
#
# Copyright 2009 Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import uuid

#import asyncio
#import tornado.escape
#import tornado.ioloop
#import tornado.locks
#import tornado.web
#from tornado.options import define, options, parse_command_line

import cyclone.web
import cyclone.options as opts

import twisted.logger

from twisted.internet import reactor
from twisted.python   import log


opts.define('port',  default=8888, help='run on the given port', type=int)
opts.define('debug', default=True, help='run in debug mode')


class MessageMixin:
    waiters = []
    cache   = []
    cache_size = 200

    def wait_for_messages(self, callback, cursor=None):
        cls = MessageMixin

        if cursor:
            index = 0

            for i in range(len(cls.cache)):
                index = len(cls.cache) - i - 1
                if cls.cache[index]['id'] == cursor:
                    break

            recent = cls.cache[index + 1:]

            if recent:
                callback(recent)
                return

        cls.waiters.append(callback)

    def new_messages(self, messages):
        cls = MessageMixin

        log.msg('Sending new message to {} listeners'.format(len(cls.waiters)))

        for callback in cls.waiters:
            try:
                callback(messages)
            except Exception:
                log.err()

        cls.waiters = []
        cls.cache.extend(messages)

        if len(cls.cache) > self.cache_size:
            cls.cache = cls.cache[-self.cache_size:]


class MainHandler(cyclone.web.RequestHandler):
    def get(self):
        self.render('index.html', messages=MessageMixin.cache)


class MessageNewHandler(cyclone.web.RequestHandler, MessageMixin):
    """Post a new message to the chat room."""

    def post(self):
        message = {
            'id'  : str(uuid.uuid4()),
            'body': self.get_argument('body'),
        }

        message['html'] = self.render_string('message.html', message=message)

        if self.get_argument('next', None):
            self.redirect(self.get_argument('next'))
        else:
            self.write(message)

        self.new_messages([message])


class MessageUpdatesHandler(cyclone.web.RequestHandler, MessageMixin):
    """Long-polling request for new messages.

    Waits until new messages are available before returning anything.
    """

    @cyclone.web.asynchronous
    def post(self):
        cursor = self.get_argument('cursor', None)
        self.wait_for_messages(self.on_new_messages, cursor=cursor)

    def on_new_messages(self, messages):
        # Closed client connection
        #if self.request.connection.stream.closed():
            #return
        self.finish(dict(messages=messages))


def main():
    app = cyclone.web.Application([
            (r'/',                  MainHandler),
            (r'/a/message/new',     MessageNewHandler),
            (r'/a/message/updates', MessageUpdatesHandler)],
        cookie_secret = '__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__',
        template_path = os.path.join(os.path.dirname(__file__), 'templates'),
        static_path   = os.path.join(os.path.dirname(__file__), 'static'),
        xsrf_cookies  = True,
        autoescape    = None,
        debug         = opts.options.debug,
    )
    reactor.listenTCP(opts.options.port, app)
    reactor.run()


"""
import zope.interface

@zope.interface.implementer(twisted.logger.ILogObserver)
class STDLibLogObserver:
    def __init__(self, name='twisted', stackDepth=1):
        self.logger = stdlibLogging.getLogger(name)
        self.logger.findCaller = self._findCaller
        self.stackDepth = stackDepth

    def _findCaller(self, stackInfo=False, stackLevel=1):
        f = currentframe(self.stackDepth)
        co = f.f_code
        if _PY3:
            extra = (None,)
        else:
            extra = ()
        return (co.co_filename, f.f_lineno, co.co_name) + extra

    def __call__(self, event):
        level = event.get("log_level", LogLevel.info)
        failure = event.get('log_failure')
        if failure is None:
            excInfo = None
        else:
            excInfo = (
                failure.type, failure.value, failure.getTracebackObject())
        stdlibLevel = toStdlibLogLevelMapping.get(level, stdlibLogging.INFO)
        self.logger.log(
            stdlibLevel, StringifiableFromEvent(event), exc_info=excInfo)
"""

if __name__ == "__main__":
    opts.parse_command_line()
#   twisted.logger.globalLogBeginner.beginLoggingTo([STDLibLogObserver()], redirectStandardIO=False)
#   log.startLogging(sys.stdout)
    main()
