# coding: utf-8
#
# Copyright 2010 Alexandre Fiori
# based on the original Tornado by Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import inspect
import urllib.parse

import twisted.internet.defer as defer
import twisted.python.log     as log


def _emit(self, eventDict):
    text = log.textFromEventDict(eventDict)
    if not text:
        return

    timeStr = self.formatTime(eventDict['time'])
    log.util.untilConcludes(self.write, "%s %s\n" % (timeStr,
                                            text.replace("\n", "\n\t")))
    log.util.untilConcludes(self.flush)

log.FileLogObserver.emit = _emit


class ObjectDict(dict):
    """Makes a dictionary behave like an object."""
    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        self[name] = value

# def import_object --> twisted.python.reflect.namedAny

bytes_type = bytes
unicode_type = str
basestring_type = str

def doctests():  # pragma: no cover
    import doctest
    return doctest.DocTestSuite()


def url_concat(url, args):
    """Concatenate url and argument dictionary regardless of whether
    url has existing query parameters.

    >>> url_concat("http://example.com/foo?a=b", dict(c="d"))
    'http://example.com/foo?a=b&c=d'
    """
    if not args:
        return url
    if url[-1] not in ('?', '&'):
        url += '&' if ('?' in url) else '?'
    return url + urllib.parse.urlencode(args)


def format_outer_frames(message=None, prefix='', limit=None, frame=None):
    frame  = frame or inspect.currentframe().f_back
    lines  = message and [message] or []
    lines += [prefix + '%s:%d:%s'%frame[1:4]
        for frame in inspect.getouterframes(frame)[:limit][::-1]]
    return '\n'.join(map(safestr, lines))


def safestr(data):
    if isinstance(data, str):
        return data

    try:
        try:
            if isinstance(data, bytes):
                data = str(data, 'utf8', 'replace')
            else:
                data = str(data)
        except UnicodeError:
            data = repr(data)
    except Exception as exc:
        data = 'FATAL ERROR IN safestr: [{}] {}'.format(type(exc).__name__, exc)

    return data


def saferepr(data):
    try:
        return repr(data)
    except Exception as exc:
        return 'FATAL ERROR IN saferepr: [{}] {}'.format(type(exc).__name__, exc)


class SingleObserver:
    """
    A helper for ".when_*()" sort of functions.

    @copyright: txtorcon by meejah 2013
    """

    NOT_FIRED = object()

    def __init__(self):
        self._observers = []
        self._fired = self.NOT_FIRED

    def when_fired(self):
        d = defer.Deferred()
        if self._fired is not self.NOT_FIRED:
            d.callback(self._fired)
        else:
            self._observers.append(d)
        return d

    def fire(self, value):
        if self._observers is None:
            return  # raise RuntimeError("already fired") ?
        self._fired = value
        for d in self._observers:
            d.callback(self._fired)
        self._observers = None
        return value  # so we're transparent if used as a callback
