# -*- coding: utf-8 -*-

import zope.interface as _zi


class ICyclone(_zi.Interface):
	"""Просто маркер, см. L{.zcml}."""


class IURLSpec(_zi.Interface):
	pass


class ICycloneRequest(_zi.Interface):
	pass


class IRequestHandler(_zi.Interface):
	pass
