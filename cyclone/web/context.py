# -*- coding: utf-8 -*-
# Copyright (c) Divmod

import zope.interface as _zi

import twisted.python.reflect as reflect


STUB = object()


class WebContext:
	def __init__(self, parent=None, tag=None, remembrances=None):
		self.tag = tag
#		sd = getattr(tag, 'slotData', None)
#		if sd is not None:
#			self._slotData = sd
		self.parent = parent
		self.__remembrances__ = remembrances

	def __repr__(self):
		rstr = ''
		if self.__remembrances__:
			rstr = ', remembrances={!r}'.format(self.__remembrances__)
		return '{}(tag={!r}{})'.format(type(self).__name__, self.tag, rstr)

	def __conform__(self, interface):
		try:
			return self.locate(interface)
		except KeyError:
			pass

	def remember(self, adapter, interface=None):
		"""Remember an object that implements some interfaces.
		Later, calls to .locate which are passed an interface implemented
		by this object will return this object.

		If the 'interface' argument is supplied, this object will only
		be remembered for this interface, and not any of
		the other interfaces it implements.
		"""

		if interface is None:
			interfaceList = [reflect.qual(iface) for iface in _zi.providedBy(adapter)]
			if not interfaceList:
				interfaceList = [dataqual]
		else:
			interfaceList = [reflect.qual(interface)]

		if self.__remembrances__ is None:
			self.__remembrances__ = {}

		for interface in interfaceList:
			self.__remembrances__[interface] = adapter

		return self

	def locate(self, interface, depth=1):
		key    = reflect.qual(interface)
		ctxcur = self

		while True:
			if depth < 0:
				full = []
				while True:
					try:
						full.append(self.locate(interface, len(full) + 1))
					except KeyError:
						break
				if full:
					return full[depth]
				break  # [bw] KeyError?

			if ctxcur.__remembrances__:
				rememberedValue = ctxcur.__remembrances__.get(key, STUB)

				if rememberedValue is not STUB:
					depth -= 1
					if not depth:
						return rememberedValue

			# hook for L{FactoryContext} and other implementations of complex locating
			locateHook = getattr(ctxcur, 'locateHook', None)

			if locateHook is not None:
				result = locateHook(interface)
				if result is not None:
					ctxcur.remember(result, interface)  # [bw] !!!?
					return result

			if ctxcur.parent is None:
				raise KeyError('Interface "{}" was not remembered'.format(key))

			ctxcur = ctxcur.parent

#	def chain(self, context):
#	def fillSlots(self, name, stan):
#	def locateSlotData(self, name):
#	def clone(self, deep=True, cloneTags=True):


class FactoryContext(WebContext):
	"""A context which allows adapters to be registered against it so that an object
	can be lazily created and returned at render time. When ctx.locate is called
	with an interface for which an adapter is registered, that adapter will be used
	and the result returned.
	"""

	inLocate = 0

	def locateHook(self, interface):
		# Prevent infinite recursion from interface(self) calling self.getComponent calling self.locate
		self.inLocate += 1
		try:
			return interface(self, None)
		finally:
			self.inLocate -= 1

	def __conform__(self, interface):
		if not self.inLocate:
			return super().__conform__(interface)


class SiteContext(FactoryContext):
	"""A SiteContext is created and installed on a NevowSite upon initialization.
	It will always be used as the root context, and can be used as a place to remember
	things sitewide.
	"""


class RequestContext(FactoryContext):
	"""A RequestContext has adapters for the following interfaces:

	- ISession
	- IFormDefaults
	- IFormErrors
	- IHand
	- IStatusMessage
	"""


class PageContext(FactoryContext):
	"""A PageContext has adapters for the following interfaces:

	- IRenderer
	- IRendererFactory
	- IData
	"""

	def __init__(self, *args, **kwarg):
		super().__init__(*args, **kwarg)
		if self.tag is not None and hasattr(self.tag, 'toremember'):
			# @see: C{.rend.Fragment}
			for obj, iface in self.tag.toremember:
				self.remember(obj, iface)
