# -*- coding: utf-8 -*-
# Copyright (c) Divmod

import zope.interface as _zi


class IResource(_zi.Interface):
	def locateChild(ctx, segments):
		"""
		Locate another object which can be adapted to IResource.

		@param ctx: The context object for the request being responded to.
		@param segments: A tuple of strings giving the remaining query segments
			to resolve into an IResource.

		@return: A two-tuple of an L{IResource} provider and a tuple giving the
			query segments which remain to be processed.  A L{Deferred} which
			is called back with such a two-tuple may also be returned.
		"""

	def renderHTTP(ctx):
		"""Render a request"""


class IRequest(_zi.Interface):
	uri      = _zi.Attribute('The full URI that was requested (includes arguments)')
	path     = _zi.Attribute('The path only (arguments not included)')
	prepath  = _zi.Attribute('Path segments that have already been handled')
	postpath = _zi.Attribute('Path segments still to be handled')


class ICanHandleException(_zi.Interface):
	def renderHTTP_exception(context, failure):
		"""Render an exception to the given request object."""

	def renderInlineException(context, reason):
		"""Return stan representing the exception, to be printed in the page,
		not replacing the page."""


class IRemainingSegments(_zi.Interface):
	"""During the URL traversal process, requesting this from the context
	will result in a tuple of the segments remaining to be processed.

	Equivalent to request.postpath in twisted.web
	"""


class ICurrentSegments(_zi.Interface):
	"""Requesting this from the context will result in a tuple of path segments
	which have been consumed to reach the current Page instance during
	the URL traversal process.

	Equivalent to request.prepath in twisted.web
	"""
