# -*- coding: utf-8 -*-
# Copyright (c) Divmod

import itertools as I
import style

import zope.component as _zc
import zope.interface as _zi

import twisted.logger
import twisted.internet.defer as defer
import twisted.internet.error as _twerr
import twisted.python.failure as failure
import twisted.web.http       as _twhttp
import twisted.web.server     as _twsrv

from .. import escape
from .. import types
from .. import util as U

from . import context
from . import inevow


ARG_STUB     = object()
ERROR_MARKER = object()
NOT_DONE_YET_MARKER = object()

LOST_CONNECTION_ERRORS = (_twerr.ConnectionDone, _twerr.ConnectionLost)


@_zi.implementer(inevow.IRequest)
class NevowRequest(_twsrv.Request):
	logger = twisted.logger.Logger()

	__counter__ = I.count(1).__next__

	__lostConnection  = False  # Nevow API
	__deferredProcess = None
	__notDoneYetResource = None
	__notDoneYetDeferred = None

	def __init__(self, channel, *args, **kwargs):
		self.ident = self.__counter__()
		super().__init__(channel, *args, **kwargs)
		self.notifyFinish().addBoth(self.__setLostConnection)

	def __repr__(self):
		return '<{}.{:03d} {} "{}">'.format(
			type(self).__name__,
			self.ident,
			escape.to_unicode(self.method),
			escape.to_unicode(self.uri))

	def __setLostConnection(self, error):
		# notified by `_twhttp.Request.finish` (queued request) or `_twhttp.Request.connectionLost`

#		print(style.white('--lostConnection--', self, 'queued={}'.format(self.queued),
#			'LOST' if isinstance(error, failure.Failure) else 'FINISH',
#			self.__deferredProcess,    getattr(self.__deferredProcess,    'called', None),
#			self.__notDoneYetDeferred, getattr(self.__notDoneYetDeferred, 'called', None),
#		))

		if isinstance(error, failure.Failure):
			self.__lostConnection = True

			if error.check(*LOST_CONNECTION_ERRORS):
				self.finish()
			else:
				self.finish(error=error)

	def setNotDoneYetResource(self, resource):
#		print(style.white('--setNotDoneYetResource--', self, resource))
		if resource is not self.__notDoneYetResource:
			assert self.__notDoneYetResource is None, (self.__notDoneYetResource, resource)
			assert self.__notDoneYetDeferred is None, (self.__notDoneYetDeferred, resource)
			self.__notDoneYetResource = resource
			self.__notDoneYetDeferred = defer.Deferred()
		return self.__notDoneYetDeferred

	def render(self, resource):
		# @note: может вызываться несколько раз, см. L{twisted.web.util.DeferredResource}
		assert self.__deferredProcess    is None
		assert self.__notDoneYetDeferred is None

		if not isinstance(resource, defer.Deferred):
			# legacy Twisted behavior
			result = super().render(resource)

			if result == _twhttp.NOT_DONE_YET:
				self.__deferredProcess = self.setNotDoneYetResource(resource)
			#else: @todo: ???

		else:
			# Nevow behavior
			self.__deferredProcess = resource
			self.__deferredProcess\
				.addCallback(self.gotPageContext)

#		print(style.white('--render--', self,
#			self.__deferredProcess,    getattr(self.__deferredProcess,    'called', None),
#			self.__notDoneYetDeferred, getattr(self.__notDoneYetDeferred, 'called', None),
#		))

		if self.__deferredProcess is not None:
			self.__deferredProcess\
				.addErrback(self.processingFailed)\
				.addBoth(self.processingDone)

	def finish(self, trick=False, error=None):
#		print(style.white('--finish--', self, trick, self.__notDoneYetResource, self.__deferredProcess,
#			'notifications={}'.format(len(self.notifications)),
#			'queued={}'.format(self.queued)),
#			style.light_black(U.format_outer_frames('\n', prefix=':\t', limit=4)))

		if self.__notDoneYetDeferred is not None:
			deferred = self.__notDoneYetDeferred

			del self.__notDoneYetDeferred
			del self.__notDoneYetResource

			if error is None:
				deferred.callback(NOT_DONE_YET_MARKER)
			else:
				deferred.errback(error)

			if not self.__lostConnection:
				super().finish()

		elif trick or self.__deferredProcess is None or self.__deferredProcess.called:
			if not self.__lostConnection:
				super().finish()

	def gotPageContext(self, ctxpage):
		if ctxpage is ERROR_MARKER:
			return ERROR_MARKER

		if not isinstance(ctxpage, context.WebContext):
			raise TypeError('Expected <PageContext>', type(ctxpage))

		resource = inevow.IResource(ctxpage.tag, None)

		if resource is None:
			raise TypeError('Expected <IResource>', type(ctxpage.tag))

		return defer.maybeDeferred(resource.renderHTTP, ctxpage)\
			.addCallback(self.cbFinishRender, ctxpage)\
			.addErrback(self.processingFailed, ctxpage)

	def processingFailed(self, reason, ctx=None):
		# @todo: check L{twisted.web.util.DeferredResource}

		try:
			handler = inevow.ICanHandleException(ctx, None)

			if handler is None:
				# default Twisted behavior
				super().processingFailed(reason)
			else:
				handler.renderHTTP_exception(ctx, reason)

		except:
			self.setResponseCode(_twhttp.INTERNAL_SERVER_ERROR)

			self.logger.failure('Exception rendering error page')
			self.logger.failure('Original exception', reason)

			if not self.finished:
				self.write(
					b'<html><head><title>Internal Server Error</title></head>'
					b'<body><h1>Internal Server Error</h1>'
					b'An error occurred rendering the requested page. '
					b'Additionally, an error occurred rendering the error page.'
					b'</body></html>')

		if self.__deferredProcess is not None:
			del self.__deferredProcess

		self.finish()

		return ERROR_MARKER

	def processingDone(self, result):
		if self.__deferredProcess is not None:
			deferred = self.__deferredProcess
			del self.__deferredProcess
			assert deferred.called
		return result

	def cbFinishRender(self, html, ctx):
		"""
		Callback for the page rendering process having completed.

		@param html: Either the content of the response body (L{bytes}) or a
			marker that an exception occurred and has already been handled or
			an object adaptable to L{IResource} to use to render the response.
		"""

		if self.__deferredProcess is not None:
			del self.__deferredProcess

		if not self.__lostConnection:
			if html is NOT_DONE_YET_MARKER:
				pass
			elif html is None:
				self.finish()
			elif isinstance(html, bytes):
				self.write(html)
				self.finish()
			elif isinstance(html, str):
				self.write(html.encode('ascii'))
				self.finish()
			elif html is not ERROR_MARKER:
				resource = inevow.IResource(html)
				ctxpage  = context.PageContext(tag=resource, parent=ctx)
				return self.gotPageContext(ctxpage)


@_zc.adapter(inevow.IRequest)
@_zi.implementer(types.ICycloneRequest)
class CycloneRequest:
	def __init__(self, original):
		self.original = original
		self.method   = escape.to_unicode(original.method)
		self.uri      = escape.to_unicode(original.uri)

	startedWriting = property(lambda self: bool(self.original.startedWriting))
	hasETag        = property(lambda self: self.original.etag is not None)

	def __repr__(self):
		return '<{}.{:03d} {} "{}">'.format(
			type(self).__name__, self.original.ident,
			self.method,         self.uri)

	@property
	def path(self):
		return escape.to_unicode(self.original.URLPath().path)  # @todo: '' vs '/' ?

	def notifyFinish(self):
		return self.original.notifyFinish()

	def setNotDoneYetResource(self, resource):
		return self.original.setNotDoneYetResource(resource)

	def write(self, data):
		self.original.write(data)

	def finish(self):
		self.original.finish()

	def hasArgument(self, name):
		return escape.utf8(name) in self.original.args

	def getArgument(self, name, default=None):
		value = self.original.args.get(escape.utf8(name), ARG_STUB)
		if value is ARG_STUB:
			return default
		if not value:
			return ''
		if isinstance(value, (tuple, list)):
			value = value[0]
		return escape.to_unicode(value)

	def setResponseCode(self, code, message=None):
		self.original.setResponseCode(code, escape.utf8(message))

	def setHeader(self, name, value):
		self.original.setHeader(name, value)

	def getHeader(self, name, default=None):
		result = self.original.getHeader(name)
		if result is None:
			return result
		if type(result) is bytes:
			return escape.to_unicode(result)
		return result

	def getCookie(self, name, default=None):
		name  = escape.utf8(name)
		value = self.original.getCookie(name)
		if value is None:
			return default
		return escape.to_unicode(value)

#	def addCookie(self, key, value, expires=None, domain=None, path=None, max_age=None, comment=None, secure=None, httpOnly=False, sameSite=None):
#		self.original.addCookie(key, value,
#			expires=expires, domain=domain, path=path, max_age=max_age,
#			comment=comment, secure=secure, httpOnly=httpOnly, sameSite=sameSite)

	def addCookie(self, cookie):
		self.original.addCookie(cookie.key, cookie.value,
			expires =cookie.get('expires'),
			domain  =cookie.get('domain'),
			path    =cookie.get('path'),
			max_age =cookie.get('max-age'),
			comment =cookie.get('comment'),
			secure  =cookie.get('secure'),
			httpOnly=cookie.get('httponly'))

	def setETag(self, etag):
		return self.original.setETag(etag)
