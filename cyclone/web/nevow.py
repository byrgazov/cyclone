# -*- coding: utf-8 -*-
# Copyright (c) 2004-2008 Divmod.


import twisted.logger
import twisted.web.resource as _twres
import twisted.web.server   as _twsrv
import twisted.web.http     as _twhttp


class NevowSite(_twsrv.Site):
	def __init__(self, resource, requestFactory=None, *args, **kwargs):
		"""
		@param resource: The root of the resource hierarchy.  All request
			traversal for requests received by this factory will begin at this
			resource.
		@type resource: L{IResource} provider
		@param requestFactory: Overwrite for default requestFactory.
		@type requestFactory: C{callable} or C{class}.

		@see: L{twisted.web.http.HTTPFactory.__init__}
		"""

		super().__init__(*args, **kwargs)

#		self.sessions = {}
		self.resource = resource
#		self.resource.addSlash = True

		if requestFactory is not None:
			self.requestFactory = requestFactory

		self.context = context.SiteContext()
