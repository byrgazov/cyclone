# -*- coding: utf-8 -*-

import itertools as I

import sys, os
import stat
import re
import functools
import types as pytypes
import numbers
import hashlib
import binascii
import uuid
import threading
import datetime, time
import calendar
import urllib.parse
import mimetypes
import http.client
import http.cookies
import email.utils

import zope.component as _zc
import zope.interface as _zi

import twisted.logger
import twisted.internet.defer as defer
import twisted.python.failure as failure
import twisted.web.http       as _twhttp

from .. import escape
from .. import locale
from .. import template
from .. import types
from .. import util

from . import inevow


STUB = object()


class HTTPError(Exception):
	"""An exception that will turn into an HTTP error response.

	:arg int status_code: HTTP status code.  Must be listed in
		`httplib.responses` unless the ``reason`` keyword argument is given.
	:arg string log_message: Message to be written to the log for this error
		(will not be shown to the user unless the `Application` is in debug
		mode).  May contain ``%s``-style placeholders, which will be filled
		in with remaining positional parameters.
	:arg string reason: Keyword-only argument.  The HTTP "reason" phrase
		to pass in the status line along with ``status_code``.  Normally
		determined automatically from ``status_code``, but can be used
		to use a non-standard numeric code.
	"""

	def __init__(self, status_code, log_message=None, *args, **kwargs):
		self.status_code = status_code
		self.log_message = log_message
		self.args   = args
		self.reason = kwargs.get('reason', None)

	def __str__(self):
		if self.log_message:
			return self.log_message % self.args
		return self.reason or http.client.responses.get(self.status_code, 'Unknown')


class HTTPAuthenticationRequired(HTTPError):
	"""An exception that will turn into an HTTP 401, Authentication Required.

	The arguments are used to compose the ``WWW-Authenticate`` header.
	See http://en.wikipedia.org/wiki/Basic_access_authentication for details.

	:arg string auth_type: Authentication type (``Basic``, ``Digest``, etc)
	:arg string realm: Realm (Usually displayed by the browser)
	"""

	def __init__(self, log_message=None, auth_type='Basic', realm='Restricted Access', **kwargs):
		self.status_code = 401
		self.log_message = log_message
		self.auth_type   = auth_type
		self.kwargs      = kwargs
		self.kwargs['realm'] = realm


@_zc.adapter(types.IRequestHandler, types.IURLSpec)
@_zi.implementer(inevow.IResource)
class CycloneHandlerAdapter:
	def __init__(self, handler, spec):
		self.spec    = spec
		self.handler = handler
		self.request = handler.request

	def locateChild(self, ctx, segments):
		pass

	def renderHTTP(self, ctx):
		if self.spec.regex.groups:
			# None-safe wrapper around url_unescape to handle
			# unmatched optional groups correctly

			def unquote(s):
				if s is None:
					return s
				return escape.url_unescape(s)

			# Pass matched groups to the handler.
			# Since match.groups() includes both named and unnamed groups,
			# we want to use either groups or groupdict but not both.
			# Note that args are passed as bytes so the handler can
			# decide what encoding to use.

			match = self.spec.regex.match(self.request.path)

			if self.spec.regex.groupindex:
				args   = ()
				kwargs = {str(k): unquote(v) for (k, v) in match.groupdict().items()}
			else:
				args   = tuple(unquote(s) for s in match.groups())
				kwargs = {}
		else:
			args   = ()
			kwargs = {}

		# in debug mode, re-compile templates and reload static files on every
		# request so you don't need to restart to see changes
		if self.handler.settings.get('debug'):
			with RequestHandler._template_loader_lock:
				for loader in RequestHandler._template_loaders.values():
					loader.reset()
			StaticFileHandler.reset()

#		application = types.ICyclone(ctx) / self.handler.application
#		request     = inevow.IRequest(ctx)
		transforms  = [transform(request) for transform in self.handler.application.transforms]

		def done(result):
			if not self.handler.auto_finish and not self.handler.finished:
				return self.request.setNotDoneYetResource(self)

		return self.handler._execute(transforms, *args, **kwargs)\
			.addCallback(done)


class UIModuleNamespace:
	"""Lazy namespace which creates UIModule proxies bound to a handler."""

	def __init__(self, handler, ui_modules):
		self.__handler = handler
		self.__modules = ui_modules

	def __getitem__(self, key):
		return self.__handler._ui_module(key, self.__modules[key])

	def __getattr__(self, key):
		try:
			return self[key]
		except KeyError as exc:
			raise AttributeError(str(exc))


def asynchronous(method):
	"""Wrap request handler methods with this if they are asynchronous.

	If this decorator is given, the response is not finished when the
	method returns. It is up to the request handler to call self.finish()
	to terminate the HTTP request. Without this decorator, the request is
	automatically finished when the get() or post() method returns. ::

		from twisted.internet import reactor

		class MyRequestHandler(web.RequestHandler):
			@web.asynchronous
			def get(self):
				self.write("Processing your request...")
				reactor.callLater(5, self.do_something)

			def do_something(self):
				self.finish("done!")

	It may be used for Comet and similar push techniques.
	http://en.wikipedia.org/wiki/Comet_(programming)
	"""
	@functools.wraps(method)
	def wrapper(self, *args, **kwargs):
		self.auto_finish = False
		return method(self, *args, **kwargs)
	return wrapper


@_zi.implementer(types.IRequestHandler)
class RequestHandler:
	_template_loader_lock = threading.Lock()
	_template_loaders = {}

	logger = twisted.logger.Logger()

	SUPPORTED_METHODS = ('GET', 'HEAD', 'POST', 'DELETE', 'PATCH', 'PUT', 'OPTIONS')
	ENTITY_HEADERS    = ('Allow', 'Content-Encoding', 'Content-Language', 'Content-Length',
		'Content-MD5', 'Content-Range', 'Content-Type', 'Last-Modified')

	__finished    = False
	__headers_written = False
	__new_cookie  = None
	__xsrf_token  = None

	auto_finish      = True  # @see: L{asynchronous}
	serialize_lists  = False
	current_user     = None
	xsrf_cookie_name = '_xsrf'

	def __init__(self, application, request, **kwargs):
		self.application = application
		self.request     = types.ICycloneRequest(request)

		# ...

		self.__transforms = []
		self.__headers = {}
		self.__write_buffer = []

		assert '_modules' not in application.ui_methods
		assert 'modules'  not in application.ui_methods

		self.ui = util.ObjectDict((name, self._ui_method(module)) for name, module in application.ui_methods.items())
		# UIModules are available as both `modules` and `_modules` in the
		# template namespace.  Historically only `modules` was available
		# but could be clobbered by user additions to the namespace.
		# The template {% module %} directive looks in `_modules` to avoid
		# possible conflicts.
		self.ui['_modules'] = UIModuleNamespace(self, application.ui_modules)
		self.ui['modules']  = self.ui['_modules']

		self.clear()
		self.initialize(**kwargs)

	@property
	def settings(self):
		return self.application.settings

	def require_setting(self, name, feature='this feature'):
		"""Raises an exception if the given app setting is not defined."""
		if not self.application.settings.get(name):
			raise Exception('You must define the "{}" setting in your application to use {}'\
				.format(name, feature))

	def initialize(self, **kwargs):
		pass

	def default(self, *args, **kwargs):
		raise HTTPError(405)

	def prepare(self):
		pass

	def on_finish(self):
		# не будет вызван при разрыве соединения с другой стороны (или по тысяче других причин)
		pass # self.logger.debug('on-finish: {request}', request=self.request)

	def on_connection_close(self):
		pass # self.logger.debug('on-connection-close: {request}', request=self.request)

	def clear(self):
		"""Resets all headers and content for this response."""
		# The performance cost of cyclone.httputil.HTTPHeaders is significant
		# (slowing down a benchmark with a trivial handler by more than 10%),
		# and its case-normalization is not generally necessary for
		# headers we generate on the server side, so use a plain dict
		# and list instead.

		self.__headers.clear()
		self.__headers.update({
			'Content-Type': 'text/html; charset=UTF-8',
		})

#		self.__list_headers = []
		self.set_default_headers()

#		if not self.request.supports_http_1_1():
#			if self.request.headers.get('Connection') == 'Keep-Alive':
#				self.set_header('Connection', 'Keep-Alive')

		del self.__write_buffer[:]

		self.__status_code = 200
		self.__status_text = http.client.responses[self.__status_code]

	def set_default_headers(self):
		pass

	def set_status(self, status_code, reason=None):
		self.__status_code = status_code

		if reason is not None:
			self.__status_text = escape.native_str(reason)
		else:
			try:
				self.__status_text = http.client.responses[status_code]
			except KeyError:
				raise ValueError('Unknown status code', status_code)

#	def get_status(self):

	def set_header(self, name, value):
		self.__headers[name] = self.__convert_header_value(value)

#	def add_header(self, name, value):
#		self.__list_headers.append((name, self.__convert_header_value(value)))

	def clear_header(self, name):
		if name in self.__headers:
			del self.__headers[name]

	def __convert_header_value(self, value):
		if isinstance(value, bytes):
			value = value.decode('utf-8')
		elif isinstance(value, str):
			pass
		elif isinstance(value, numbers.Integral):
			# return immediately since we know the converted value will be safe
			return str(value)
		elif isinstance(value, datetime.datetime):
			t = calendar.timegm(value.utctimetuple())
			return email.utils.formatdate(t, localtime=False, usegmt=True)
		else:
			raise TypeError('Unsupported header value', type(value))

		# If \n is allowed into the header, it is possible to inject
		# additional headers or split the request. Also cap length to
		# prevent obviously erroneous values.
		if 4000 < len(value) or re.search(r'[\x00-\x1f]', value):
			raise ValueError('Unsafe header value', type(value))

		return value

	def get_argument(self, name, default=STUB, strip=True):
		value = self.request.getArgument(name, default)
		if value is STUB:
			raise HTTPError(400, 'Missing argument %s', name)
		if type(value) is str:
			return value.strip() if strip else value
		return value

#	def get_arguments(self, name, strip=True):

	def decode_argument(self, value, name=None):
		return escape._unicode(value)

#	@property
#	def cookies(self):

	def get_cookie(self, name, default=None):
		return self.request.getCookie(name, default)

	def set_cookie(self, name, value, domain=None, expires=None, path='/', expires_days=None, **kwargs):
		"""Sets the given cookie name/value with the given options.

		Additional keyword arguments are set on the Cookie.Morsel directly.
		See http://docs.python.org/library/cookie.html#morsel-objects
		for available attributes.
		"""

		name  = escape.native_str(name)
		value = escape.native_str(value)

		if re.search(r'[\x00-\x20]', name + value):
			# Don't let us accidentally inject bad stuff
			raise ValueError('Invalid cookie {!r}: {!r}'.format(name, value))

		if self.__new_cookie is None:
			self.__new_cookie = http.cookies.SimpleCookie()

		if name in self.__new_cookie:
			del self.__new_cookie[name]

		self.__new_cookie[name] = value
		morsel = self.__new_cookie[name]

		if domain:
			morsel['domain'] = domain

		if expires_days is not None and not expires:
			expires = datetime.datetime.utcnow() + datetime.timedelta(days=expires_days)

		if expires:
			timestamp = calendar.timegm(expires.utctimetuple())
			morsel['expires'] = email.utils.formatdate(timestamp, localtime=False, usegmt=True)

		if path:
			morsel['path'] = path

		for key, value in kwargs.items():
			if key == 'max_age':
				key = 'max-age'
			morsel[key] = value

#	def clear_cookie(self, name, path='/', domain=None):
#	def clear_all_cookies(self):
#	def set_secure_cookie(self, name, value, expires_days=30, **kwargs):
#	def create_signed_value(self, name, value):
#	def get_secure_cookie(self, name, value=None, max_age_days=31):

	def redirect(self, url, permanent=False, status=None):
		if self.__headers_written:
			raise Exception('Cannot redirect after headers have been written')

		if status is None:
			status = 301 if permanent else 302
		else:
			assert isinstance(status, int) and 300 <= status <= 399

		self.set_status(status)

		# remove whitespace
		url = re.sub(r'[\x00-\x20]+', '', url)

		if not self.request.uri.startswith('/'):
			request_uri = ''

		if self.request.uri.startswith('//'):
			request_uri = ''
		else:
			request_uri = self.request.uri

		self.set_header('Location', urllib.parse.urljoin(request_uri, url))
		self.finish()

	def render(self, template_name, **kwargs):
		"""Renders the template with the given arguments as the response."""
		return defer.maybeDeferred(self.render_string, template_name, **kwargs)\
			.addCallback(self._insertAdditionalPageElements)\
			.addCallbacks(self.finish, self._execute_failure)

	def render_string(self, template_name, **kwargs):
		"""Generate the given template with the given arguments.

		We return the generated string. To generate and write a template
		as a response, use render() above.
		"""

		# If no template_path is specified, use the path of the calling file

		template_path = self.get_template_path()

		if not template_path:
			frame = sys._getframe(0)
			web_file = frame.f_code.co_filename
			while frame.f_code.co_filename == web_file:
				frame = frame.f_back
			template_path = os.path.dirname(frame.f_code.co_filename)

		with RequestHandler._template_loader_lock:
			if template_path not in RequestHandler._template_loaders:
				loader = self.create_template_loader(template_path)
				RequestHandler._template_loaders[template_path] = loader
			else:
				loader = RequestHandler._template_loaders[template_path]

		template = loader.load(template_name)
		namespace = self.get_template_namespace()
		namespace.update(kwargs)
		return template.generate(**namespace)

	def get_template_path(self):
		return self.application.settings.get('template_path')

	def get_template_namespace(self):
		namespace = dict(
			handler=self,
			request=self.request,
#			current_user=self.current_user,
			locale=self.locale,
			_     =self.locale.translate,
			static_url=self.static_url,
			xsrf_form_html=self.xsrf_form_html,
#			reverse_url=self.reverse_url
		)
		namespace.update(self.ui)
		return namespace

	def create_template_loader(self, template_path):
		settings = self.application.settings

		if 'template_loader' in settings:
			return settings['template_loader']

		kwargs = {}

		if 'autoescape' in settings:
			# autoescape=None means "no escaping", so we have to be sure
			# to only pass this kwarg if the user asked for it.
			kwargs['autoescape'] = settings['autoescape']

		return template.Loader(template_path, **kwargs)

	@property
	def xsrf_token(self):
		if self.__xsrf_token is None:
			token = self.get_cookie(self.xsrf_cookie_name)
			if not token:
				token = binascii.b2a_hex(uuid.uuid4().bytes)
				expires_days = 30 if self.current_user else None
				self.set_cookie(self.xsrf_cookie_name, token, expires_days=expires_days)
			self.__xsrf_token = token
		return self.__xsrf_token

	def check_xsrf_cookie(self):
		token = self.get_argument(self.xsrf_cookie_name, None)\
		     or self.request.headers.get('X-Xsrftoken')       \
		     or self.request.headers.get('X-Csrftoken')

		if not token:
			raise HTTPError(403, '"_xsrf" argument missing from POST')

		if self.xsrf_token != token:
			raise HTTPError(403, 'XSRF cookie does not match POST argument')

	def xsrf_form_html(self):
		return '<input type="hidden" name="' + self.xsrf_cookie_name + \
			'" value="' + escape.xhtml_escape(self.xsrf_token) + '"/>'

	def static_url(self, path, include_host=None):
		self.require_setting('static_path', 'static_url')

		static_handler_class = self.settings.get('static_handler_class', StaticFileHandler)

		if include_host is None:
			include_host = getattr(self, 'include_host', False)

		if include_host:
			base = self.request.protocol + '://' + self.request.host
		else:
			base = ''

		return base + static_handler_class.make_static_url(self.settings, path)

	@property
	def locale(self):
		if not hasattr(self, '_locale'):
			self._locale = self.get_user_locale()
			if not self._locale:
				self._locale = self.get_browser_locale()
				assert self._locale
		return self._locale

	def get_user_locale(self):
		pass

	def get_browser_locale(self, default='en_US'):
		"""Determines the user's locale from Accept-Language header.

		See http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.4
		"""

		languages = self.request.getHeader('Accept-Language')

		if languages:
			locales = []

			for language in languages.split(','):
				parts = language.strip().split(';')

				if 1 < len(parts) and parts[1].startswith('q='):
					try:
						score = float(parts[1][2:])
					except (ValueError, TypeError):
						score = 0.0
				else:
					score = 1.0

				locales.append((parts[0], score))

			if locales:
				locales.sort(key=lambda pair: pair[1], reverse=True)
				codes = [l[0] for l in locales]
				return locale.get(*codes)

		return locale.get(default)

	def write(self, chunk):
		if self.__finished:
			raise RuntimeError('Cannot write() after finish(). '\
				'May be caused by using async operations without the @asynchronous decorator')

		if isinstance(chunk, dict) or self.serialize_lists and isinstance(chunk, list):
			chunk = escape.json_encode(chunk)
			self.set_header('Content-Type', 'application/json')

		self.__write_buffer.append(escape.utf8(chunk))

	def flush(self, include_footers=False):
		"""Flushes the current output buffer to the network."""

		chunk = b''.join(self.__write_buffer)
		del self.__write_buffer[:]

		if not self.__headers_written:
			self.__headers_written = True

			assert not self.request.startedWriting

			for transform in self.__transforms:
				self.__status_code, self.__headers, chunk = transform.transform_first_chunk(
					self.__status_code, self.__headers, chunk, include_footers)

#			headers = self._generate_headers()

			self.request.setResponseCode(self.__status_code, self.__status_text)

			for name, value in self.__headers.items():
				self.request.setHeader(name, value)

			#for name, value in self.__list_headers: ...

			if self.__new_cookie is not None:
				for cookie in self.__new_cookie.values():
					self.request.addCookie(cookie)

		else:
			for transform in self.__transforms:
				chunk = transform.transform_chunk(chunk, include_footers)

#			headers = b''

		# Ignore the chunk and only write the headers for HEAD requests

#		if self.request.method == 'HEAD':
#			if headers:
#				self.request.write(headers)
#		else:
#			if headers or chunk:
#				self.request.write(headers + chunk)

		if self.request.method != 'HEAD' and chunk:
			self.request.write(chunk)
#		else:
#			self.request.write(b'')

	finished = property(lambda self: self.__finished)

	def finish(self, chunk=None):
		"""Finishes this response, ending the HTTP request."""

		if self.__finished:
			raise RuntimeError('finish() called twice. May be caused by using async '\
				'operations without the @asynchronous decorator')

		if chunk is not None:
			self.write(chunk)

		# automatically support ETags and add the Content-Length header if
		# we have not flushed any content yet.

		if not self.__headers_written:
			if self.__status_code == 200 and self.request.method in ('GET', 'HEAD') and not self.request.hasETag:
				etag = self.compute_etag()

				if self.request.setETag(etag) == _twhttp.CACHED:
					del self.__write_buffer[:]
					self.set_status(304)  # @xxx: `setETag` уже это сделал

			if self.__status_code == 304:
				assert not self.__write_buffer, 'Cannot send body with 304'

				for header in self.ENTITY_HEADERS:
					self.clear_header(header)

			elif 'Content-Length' not in self.__headers:
				content_length = sum(len(part) for part in self.__write_buffer)
				self.set_header('Content-Length', content_length)

		self.flush(include_footers=True)

		if not self.auto_finish:
			# @note: [bw] только при NOT_DONE_YET / @asynchronous
			self.request.finish()

#		self._log()
		self.__finished = True
		self.on_finish()

	def send_error(self, status_code=500, **kwargs):
		"""Sends the given HTTP error code to the browser.

		If `flush()` has already been called, it is not possible to send
		an error, so this method will simply terminate the response.
		If output has been written but not yet flushed, it will be discarded
		and replaced with the error page.

		Override `write_error()` to customize the error page that is returned.
		Additional keyword arguments are passed through to `write_error`.
		"""

		if self.__headers_written:
			self.logger.error('Cannot send error response after headers written')
			if not self.__finished:
				self.finish()

		else:
			self.clear()

			reason = None

			if 'exc_info' in kwargs:
				exc = kwargs['exc_info'][1]
				if isinstance(exc, HTTPError) and exc.reason:
					reason = exc.reason

			elif 'exception' in kwargs:
				exc = kwargs['exception']
				if isinstance(exc, HTTPAuthenticationRequired):
					args = ','.join(['{}="{}"'.format(key, vvalue) for key, value in exc.kwargs.items()])
					self.set_header('WWW-Authenticate', '{} {}'.format(exc.auth_type, args))

			self.set_status(status_code, reason=reason)

			try:
				self.write_error(**kwargs)
			except Exception as exc:
				self.logger.error('Uncaught exception in write_error: {exception}', exception=exc)

			if not self.__finished:
				self.finish()

	def write_error(self, **kwargs):
		if hasattr(self, 'get_error_html'):
			if 'exc_info' in kwargs:
				exc_info = kwargs.pop('exc_info')
				kwargs['exception'] = exc_info[1]
				try:
					# Put the traceback into sys.exc_info()
					#raise exc_info[0], exc_info[1], exc_info[2]
					raise exc_info[0].with_traceback(exc_info[1], exc_info[2])
				except Exception:
					self.finish(self.get_error_html(self.__status_code, **kwargs))
			else:
				self.finish(self.get_error_html(self.__status_code, **kwargs))

		elif self.settings.get('debug') and 'exc_info' in kwargs:
			# in debug mode, try to send a traceback
			self.set_header('Content-Type', 'text/plain')
			for line in traceback.format_exception(*kwargs['exc_info']):
				self.write(line)
			self.finish()

		else:
			self.set_header('Content-Type', 'text/html; charset=UTF-8')
			self.finish('<html><title>{code}: {message}</title>'\
				'<body>{code}: {message}</body></html>'\
				.format(code=self.__status_code, message=self.__status_text))

	def compute_etag(self):
		hasher = hashlib.sha1()
		for part in self.__write_buffer:
			hasher.update(part)
		return '"{}"'.format(hasher.hexdigest())

	def _execute(self, transforms, *args, **kwargs):
		"""Executes this request with the given output transforms."""
		self.__transforms[:] = transforms

		if self.request.method not in self.SUPPORTED_METHODS:
			raise HTTPError(405)

		# if XSRF cookies are turned on, reject form submissions without the proper cookie
		if self.request.method not in ('GET', 'HEAD', 'OPTIONS') and self.application.settings.get('xsrf_cookies'):
			if not getattr(self, 'no_xsrf', False):
				self.check_xsrf_cookie()

		return defer.maybeDeferred(self.prepare)\
			.addCallback(self._execute_handler, args, kwargs)

#		except Exception as exc:
#			self._handle_request_exception(exc)

	def _execute_handler(self, _, args, kwargs):
		if not self.__finished:
			args   = tuple(self.decode_argument(arg) for arg in args)
			kwargs = {key: self.decode_argument(value, name=key) for (key, value) in kwargs.items()}
			function = getattr(self, self.request.method.lower(), self.default)

			self.request.notifyFinish()\
				.addBoth(lambda _: self.on_connection_close())

			# @todo: async function

			return self._deferred_handler(function, *args, **kwargs)\
				.addCallback(self._execute_success)\
				.addErrback(self._execute_failure)

	def _deferred_handler(self, function, *args, **kwargs):
		try:
			result = function(*args, **kwargs)
		except Exception as exc:
			return defer.fail()

		if isinstance(result, failure.Failure):
			return defer.fail(result)

		if isinstance(result, defer.Deferred):
			return result

		if isinstance(result, pytypes.GeneratorType):
			# This may degrade performance a bit, but at least avoid the
			# server from breaking when someone call yield without
			# decorating their handler with @inlineCallbacks.
			self.logger.warning('{function} returned a generator. Perhaps it should be decorated with @inlineCallbacks',
				function=function)
			return self._deferred_handler(defer.inlineCallbacks(function), *args, **kwargs)

		return defer.succeed(result)

	def _execute_success(self, _):
		if self.auto_finish and not self.__finished:
			return self.finish()

	def _execute_failure(self, error):
		return self._handle_request_exception(error)

	def _handle_request_exception(self, error):
		if isinstance(error, failure.Failure):
			try:
				# these are normally twisted.python.failure.Failure
				if isinstance(error.value, (template.TemplateError, HTTPError, HTTPAuthenticationRequired)):
					error = error.value
			except Exception:
				pass

		if isinstance(error, template.TemplateError):
			self.logger.error('Request exception: {error}', error=error)
			self.send_error(500, exception=error)

		elif isinstance(error, (HTTPError, HTTPAuthenticationRequired)):
			if error.log_message and self.settings.get('debug') is True:
				self.logger.error('Request exception: {error}', error=error)

			if error.status_code not in http.client.responses:
				self.logger.error('Bad HTTP status code: {code}', code=error.status_code)
				error.status_code = 500

			self.send_error(error.status_code, exception=error)

		else:
			self.logger.error('Uncaught exception: {error}', error=error)
			if self.settings.get('debug'):
				self.logger.error('Request: {request!r}', request=self.request)

			self.send_error(500, exception=error)

	def _insertAdditionalPageElements(self, html):
		"""Insert the additional JS and CSS added by the modules on the page"""

		js_embed = []
		js_files = []
		css_embed = []
		css_files = []
		html_heads = []
		html_bodies = []

		for module in getattr(self, '_active_modules', {}).values():
			embed_part = module.embedded_javascript()

			if embed_part:
				js_embed.append(utf8(embed_part))

			file_part = module.javascript_files()

			if file_part:
				if isinstance(file_part, (str, bytes)):
					js_files.append(file_part)
				else:
					js_files.extend(file_part)

			embed_part = module.embedded_css()

			if embed_part:
				css_embed.append(utf8(embed_part))

			file_part = module.css_files()

			if file_part:
				if isinstance(file_part, (str, bytes)):
					css_files.append(file_part)
				else:
					css_files.extend(file_part)

			head_part = module.html_head()

			if head_part:
				html_heads.append(utf8(head_part))

			body_part = module.html_body()

			if body_part:
				html_bodies.append(utf8(body_part))

		def is_absolute(path):
			return any(path.startswith(x) for x in ['/', 'http:', 'https:'])

		if js_files:
			# Maintain order of JavaScript files given by modules
			paths = []
			unique_paths = set()
			for path in js_files:
				if not is_absolute(path):
					path = self.static_url(path)
				if path not in unique_paths:
					paths.append(path)
					unique_paths.add(path)
			js = ''.join('<script src="' + escape.xhtml_escape(p) + '" type="text/javascript"></script>' for p in paths)
			sloc = html.rindex('</body>')
			html = html[:sloc] + utf8(js) + '\n' + html[sloc:]

		if js_embed:
			js = '<script type="text/javascript">\n//<![CDATA[\n' + '\n'.join(js_embed) + '\n//]]>\n</script>'
			sloc = html.rindex('</body>')
			html = html[:sloc] + js + '\n' + html[sloc:]

		if css_files:
			paths = []
			unique_paths = set()
			for path in css_files:
				if not is_absolute(path):
					path = self.static_url(path)
				if path not in unique_paths:
					paths.append(path)
					unique_paths.add(path)
			css = ''.join('<link href="' + escape.xhtml_escape(p) + '" type="text/css" rel="stylesheet"/>' for p in paths)
			hloc = html.index('</head>')
			html = html[:hloc] + utf8(css) + '\n' + html[hloc:]

		if css_embed:
			css = '<style type="text/css">\n' + '\n'.join(css_embed) + '\n</style>'
			hloc = html.index('</head>')
			html = html[:hloc] + css + '\n' + html[hloc:]

		if html_heads:
			hloc = html.index('</head>')
			html = html[:hloc] + ''.join(html_heads) + '\n' + html[hloc:]

		if html_bodies:
			hloc = html.index('</body>')
			html = html[:hloc] + ''.join(html_bodies) + '\n' + html[hloc:]

		return html

	def _ui_module(self, name, module):
		def render(*args, **kwargs):
			if not hasattr(self, '_active_modules'):
				self._active_modules = {}

			if name not in self._active_modules:
				self._active_modules[name] = module(self)

			rendered = self._active_modules[name].render(*args, **kwargs)
			return rendered
		return render

	def _ui_method(self, method):
		return lambda *args, **kwargs: method(self, *args, **kwargs)


class StaticFileHandler(RequestHandler):
	CACHE_MAX_AGE = 86400 * 365 * 10   # 10 years

	__lock = threading.Lock()
	__static_hashes = {}

	@classmethod
	def reset(cls):
		with cls.__lock:
			cls.__static_hashes.clear()

	@classmethod
	def make_static_url(cls, settings, path):
		static_url_prefix = settings.get('static_url_prefix', '/static/')
		version_hash = cls.get_version(settings, path)

		if version_hash:
			return '{}{}?v={}'.format(static_url_prefix, path, version_hash)

		return '{}{}'.format(static_url_prefix, path)

	@classmethod
	def get_version(cls, settings, path):
		abs_path = os.path.join(settings['static_path'], path)

		with cls.__lock:
			hashes = cls.__static_hashes

			if abs_path not in hashes:
				try:
					with open(abs_path, 'rb') as f:
						hashes[abs_path] = hashlib.md5(f.read()).hexdigest()
				except Exception:
					self.logger.error('Could not open static file "{path}"', path=path)
					hashes[abs_path] = None

			hsh = hashes.get(abs_path)

			if hsh:
				return hsh[:5]

	def initialize(self, path, default_filename=None):
		self.root = '{}{}'.format(os.path.abspath(path), os.path.sep)
		self.default_filename = default_filename

	def head(self, path):
		self.get(path, include_body=False)

	def get(self, path, include_body=True):
		# @todo: (!) в Tornado есть обработка "Range"

		path    = self.parse_url_path(path)
		abspath = os.path.abspath(os.path.join(self.root, path))

		# os.path.abspath strips a trailing /
		# it needs to be temporarily added back for requests to root/
		if not (abspath + os.path.sep).startswith(self.root):
			raise HTTPError(403, '%s is not in root static directory', path)

		if os.path.isdir(abspath) and self.default_filename is not None:
			# need to look at the request.path here for when path is empty
			# but there is some prefix to the path that was already
			# trimmed by the routing
			if not self.request.path.endswith('/'):
				self.redirect(self.request.path + '/')
			abspath = os.path.join(abspath, self.default_filename)

		if not os.path.exists(abspath):
			raise HTTPError(404)

		if not os.path.isfile(abspath):
			raise HTTPError(403, '%s is not a file', path)

		stat_result = os.stat(abspath)
		modified = datetime.datetime.fromtimestamp(stat_result[stat.ST_MTIME])

		self.set_header('Last-Modified', modified)

		mime_type, encoding = mimetypes.guess_type(abspath)
		if mime_type:
			self.set_header('Content-Type', mime_type)

		cache_time = self.get_cache_time(path, modified, mime_type)

		if 0 < cache_time:
			self.set_header('Expires', str(datetime.datetime.utcnow() + datetime.timedelta(seconds=cache_time)))
			self.set_header('Cache-Control', 'max-age={}'.format(cache_time))

		self.set_extra_headers(path)

		# Check the If-Modified-Since, and don't send the result if the content has not been modified
		ims_value = self.request.getHeader('If-Modified-Since')

		if ims_value is not None:
			date_tuple = email.utils.parsedate(ims_value)
			if_since = datetime.datetime.fromtimestamp(time.mktime(date_tuple))
			if if_since >= modified:
				self.set_status(304)
				return

		with open(abspath, 'rb') as file:
			data = file.read()
			if include_body:
				self.write(data)
			else:
				assert self.request.method == 'HEAD'
				self.set_header('Content-Length', len(data))

	def parse_url_path(self, url_path):
		if os.path.sep != '/':
			url_path = url_path.replace('/', os.path.sep)
		return url_path

	def set_extra_headers(self, path):
		pass

	def get_cache_time(self, path, modified, mime_type):
		return self.CACHE_MAX_AGE if self.request.hasArgument('v') else 0
