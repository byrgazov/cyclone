# -*- coding: utf-8 -*-

import collections as C

import sys
import re
import urllib.parse as urllib_parse

try:
	import style
except ImportError:
	style = None

import zope.interface as _zi

import twisted.logger
import twisted.internet.defer as defer
import twisted.python.log     as _twpylog
import twisted.python.reflect as reflect
import twisted.python.urlpath as _twurl
import twisted.web.resource   as _twres
import twisted.web.server     as _twsrv

from .. import escape
from .. import types
from .. import util
from .. import zcml

from ..options import options

from . import context
from . import handlers as _handlers
from . import inevow
from . import request  as _request
from . import resource


class Application(_twsrv.Site):
	logger = twisted.logger.Logger()

	error_handler  = None
	requestFactory = _request.NevowRequest

	def __init__(self, handlers, default_host='', transforms=None, error_handler=None, reactor=None, **settings):
		assert error_handler is None, ('not implemented', error_handler)

		zcml.loadFallbackIfRequired()

		if transforms is None:
			self.transforms = []
#			if settings.get('gzip'):
#				self.transforms.append(GZipContentEncoding)
#			self.transforms.append(ChunkedTransferEncoding)
		else:
			self.transforms = transforms

		# @todo: settings = {cookie_secret, template_path, xsrf_cookies, autoescape, debug...}

#		if transforms is None: ...

		self.handlers = []
		self.named_handlers = {}
#		self.error_handler = error_handler or ErrorHandler
		self.default_host = default_host

		self.settings = util.ObjectDict(settings)

		self.ui_modules = {'linkify': _linkify, 'xsrf_form_html': _xsrf_form_html, 'Template': TemplateModule}
		self.ui_methods = {}
#		self._load_ui_modules(settings.get('ui_modules', {}))
#		self._load_ui_methods(settings.get('ui_methods', {}))

		if 'static_path' in self.settings:
			# @todo: (?) pkg_resources & foolscap/remote

			static_path       = self.settings['static_path']
			static_url_prefix = settings.get('static_url_prefix', '/static/')

			static_handler_class = settings.get('static_handler_class', _handlers.StaticFileHandler)
			static_handler_args  = settings.get('static_handler_args', {})
			static_handler_args['path'] = static_path

			handlers = list(handlers or [])

			for pattern in [re.escape(static_url_prefix) + r'(.*)', r'/(favicon\.ico)', r'/(robots\.txt)']:
				handlers.insert(0, (pattern, static_handler_class, static_handler_args))

		if handlers:
			self.addHandlers('.*$', handlers)

		root = resource.RootResource(self)
		super().__init__(root, requestFactory=None, reactor=reactor)

		self.context = context.SiteContext()

	def addHandlers(self, host_pattern, host_handlers):
		"""Appends the given handlers to our handler list.

		Host patterns are processed sequentially in the order they were
		added. All matching patterns will be considered.
		"""

		if not host_pattern.endswith('$'):
			host_pattern += '$'  # @xxx: ...

		handlers = []

		# The handlers with the wildcard host_pattern are a special
		# case - they're added in the constructor but should have lower
		# precedence than the more-precise handlers added later.
		# If a wildcard handler group exists, it should always be last
		# in the list, so insert new groups just before it.

		if self.handlers and self.handlers[-1][0].pattern == '.*$':
			self.handlers.insert(-1, (re.compile(host_pattern), handlers))
		else:
			self.handlers.append((re.compile(host_pattern), handlers))

		for spec in host_handlers:
			if isinstance(spec, tuple):
				assert len(spec) in (2, 3), spec
				pattern = spec[0]
				handler = spec[1]

				if isinstance(handler, str):
					# import the Module and instantiate the class
					# Must be a fully qualified name (module.ClassName)
					try:
#						handler = util.import_object(handler)
						handler = reflect.namedAny(handler)
					except ImportError as exc:
						self._reactor.callWhenRunning(self.logger.error,
							'Unable to load handler "{handler}" for "{pattern}": {exception}',
							handler=handler, pattern=pattern, exception=exc)
						continue

				if len(spec) == 3:
					kwargs = spec[2]
				else:
					kwargs = {}

				spec = URLSpec(pattern, handler, kwargs)

			assert isinstance(spec, URLSpec), spec

			handlers.append(spec)

			if spec.name:
				if spec.name in self.named_handlers:
					self.logger.warn('Multiple handlers named {spec}; replacing previous value', spec=spec.name)
				self.named_handlers[spec.name] = spec

	add_handlers = addHandlers

	def getHostHandlers(self, request):
		if isinstance(request, _twurl.URLPath):
			host    = request.decode('ascii').lower()
			xrealip = None
		else:
			host    = request.getRequestHostname().decode('ascii').lower()
			xrealip = request.getHeader('X-Real-Ip')

		matches = []

		for pattern, handlers in self.handlers:
			if pattern.match(host):
				matches.extend(handlers)

		# look for default host if not behind load balancer (for debugging)

		if not matches and not xrealip:
			for pattern, handlers in self.handlers:
				if pattern.match(self.default_host):
					matches.extend(handlers)

		return matches if matches else None

	def getResourceFor(self, request):
#		assert _twres.IResource.providedBy(self.resource), self.resource
#		resource = super().getResourceFor(request)
#		return resource

		# @todo: L{_twsrv.Request.process} -> C{_IEncodingResource.providedBy(_IEncodingResource)}

		# @todo: (?) inevow.IResource (locateChild + renderHTTP)
		# @todo: (?) Twisted Resource vs Nevow Resource

		# @see: L{nevow.appserver.NevowRequest.process}
		# @see: L{nevow.appserver.NevowSite.getPageContextForRequestContext}

		segments = tuple(request.postpath)

		ctxreq = context.RequestContext(parent=self.context, tag=request)
		ctxreq.remember((), inevow.ICurrentSegments)
		ctxreq.remember(segments, inevow.IRemainingSegments)

		resource = inevow.IResource(self.resource)
		ctxpage  = context.PageContext(tag=resource, parent=ctxreq)

		return defer.maybeDeferred(resource.locateChild, ctxpage, segments)\
			.addErrback(request.processingFailed, ctxreq)\
			.addCallback(self.handleSegment, ctxreq.tag, segments, ctxpage)

	def handleSegment(self, result, request, path, ctxpage):
		if result is _request.ERROR_MARKER:
			return _request.ERROR_MARKER

		if isinstance(resource, C.Iterable) and len(resource) == 2:
			newres, newpath = result
		else:
			newres, newpath = result, ()

		# if the child resource is None then display a 404 page
		if newres is None:
#			from .rend import FourOhFour
#			return context.PageContext(tag=FourOhFour(), parent=ctxpage)
			return context.PageContext(tag=inevow.IResource(_twres.NoResource()), parent=ctxpage)

		# if we got a deferred then we need to call back later, once the
		# child is actually available.
		if isinstance(newres, defer.Deferred):
			return newres.addCallback(lambda actualRes:
				self.handleSegment((actualRes, newpath), request, path, ctxpage))

		# FIX A GIANT LEAK. Is this code really useful anyway?

		newres = inevow.IResource(newres)#, persist=True)

		if newres is ctxpage.tag:
			assert not newpath is path, 'URL traversal cycle detected when attempting '\
				'to locateChild {!r} from resource {!r}'.format(path, pageContext.tag)
			assert len(newpath) < len(path), 'Infinite loop impending...'

		# we found a Resource... update the request.prepath and postpath
		for _ in range(len(path) - len(newpath)):
			if request.postpath:
				request.prepath.append(request.postpath.pop(0))

		# create a context object to represent this new resource
		ctx = context.PageContext(tag=newres, parent=ctxpage)
		ctx.remember(tuple(request.prepath), inevow.ICurrentSegments)
		ctx.remember(tuple(request.postpath), inevow.IRemainingSegments)

		if not newpath:
			return ctx

		return defer.maybeDeferred(newres.locateChild, ctx, newpath)\
			.addErrback(request.processingFailed, ctx)\
			.addCallback(self.handleSegment, request, newpath, ctx)

	#{ HTTPFactory

	log_colorize = None
	log_color    = 'white'
	log_prefix   = ''

	def startFactory(self):
		super().startFactory()
		if self.logFile is _twpylog.logfile:
			self.logFile = sys.stdout

			if options.log_colorize is False or style is None or not hasattr(style, self.log_color):
				self.log_colorize = None
			elif options.log_colorize is True or getattr(self.logFile, 'isatty', lambda: False)():
				self.log_colorize = getattr(style, self.log_color)

	def stopFactory(self):
		if self.logFile is sys.stdout:
			del self.logFile
		super().stopFactory()

	def log(self, request):
		if self.logFile is sys.stdout:
			line = self.log_prefix + self._logFormatter(self._logDateTime, request) + '\n'
			if self.log_colorize:
				line = str(self.log_colorize(line))
			self.logFile.write(line)
		else:
			super().log(request)


class UIModule:
	"""A re-usable, modular UI unit on a page.

	UI modules often execute additional queries, and they can include
	additional CSS and JavaScript that will be included in the output
	page, which is automatically inserted on page render.

	Subclasses of UIModule must override the `render` method.
	"""

	def __init__(self, handler):
		self.handler = handler
		self.request = handler.request
		self.ui      = handler.ui
		self.locale  = handler.locale

	@property
	def current_user(self):
		return self.handler.current_user

	def render(self, *args, **kwargs):
		"""Override in subclasses to return this module's output."""
		raise NotImplementedError()

	def embedded_javascript(self):
		"""Override to return a JavaScript string to be embedded in the page."""

	def javascript_files(self):
		"""Override to return a list of JavaScript files needed by this module.

	If the return values are relative paths, they will be passed to
	`RequestHandler.static_url`; otherwise they will be used as-is."""

	def embedded_css(self):
		"""Override to return a CSS string that will be embedded in the page."""

	def css_files(self):
		"""Override to returns a list of CSS files required by this module.

	If the return values are relative paths, they will be passed to
	`RequestHandler.static_url`; otherwise they will be used as-is."""

	def html_head(self):
		"""Override to return an HTML string that will be put in the <head/> element."""

	def html_body(self):
		"""Override to return an HTML string that will be put at the end of the <body/> element."""

	def render_string(self, path, **kwargs):
		"""Renders a template and returns it as a string."""
		return self.handler.render_string(path, **kwargs)


class _linkify(UIModule):
	def render(self, text, **kwargs):
		return escape.linkify(text, **kwargs)


class _xsrf_form_html(UIModule):
	def render(self):
		return self.handler.xsrf_form_html()


class TemplateModule(UIModule):
	"""UIModule that simply renders the given template.

	{% module Template("foo.html") %} is similar to {% include "foo.html" %},
	but the module version gets its own namespace (with kwargs passed to
	Template()) instead of inheriting the outer template's namespace.

	Templates rendered through this module also get access to UIModule's
	automatic javascript/css features.  Simply call set_resources
	inside the template and give it keyword arguments corresponding to
	the methods on UIModule: {{ set_resources(js_files=static_url("my.js")) }}
	Note that these resources are output once per template file, not once
	per instantiation of the template, so they must not depend on
	any arguments to the template."""

	def __init__(self, handler):
		super().__init__(handler)
		# keep resources in both a list and a dict to preserve order
		self._resource_list = []
		self._resource_dict = {}

	def render(self, path, **kwargs):
		def set_resources(**kwargs):
			if path not in self._resource_dict:
				self._resource_list.append(kwargs)
				self._resource_dict[path] = kwargs
			else:
				if self._resource_dict[path] != kwargs:
					raise ValueError('`set_resources` called with different resources for the same template')
			return ''
		return self.render_string(path, set_resources=set_resources, **kwargs)

	def _get_resources(self, key):
		return (r[key] for r in self._resource_list if key in r)

	def embedded_javascript(self):
		return '\n'.join(self._get_resources('embedded_javascript'))

	def javascript_files(self):
		result = []
		for f in self._get_resources('javascript_files'):
			if isinstance(f, (str, bytes)):
				result.append(f)
			else:
				result.extend(f)
		return result

	def embedded_css(self):
		return '\n'.join(self._get_resources('embedded_css'))

	def css_files(self):
		result = []
		for f in self._get_resources('css_files'):
			if isinstance(f, (str, bytes)):
				result.append(f)
			else:
				result.extend(f)
		return result

	def html_head(self):
		return ''.join(self._get_resources('html_head'))

	def html_body(self):
		return ''.join(self._get_resources('html_body'))


class URLReverseError(Exception):
	"""Error generating reversed URL."""


@_zi.implementer(types.IURLSpec)
class URLSpec:
	"""Specifies mappings between URLs and handlers."""

	def __init__(self, pattern, handler_class, kwargs=None, name=None):
		"""Creates a URLSpec.

		Parameters:

		pattern: Regular expression to be matched.  Any groups in the regex
			will be passed in to the handler's get/post/etc methods as
			arguments.

		handler_class: RequestHandler subclass to be invoked.

		kwargs (optional): A dictionary of additional arguments to be passed
			to the handler's constructor.

		name (optional): A name for this handler. Used by Application.reverse_url.
		"""

		if not pattern.endswith('$'):
			pattern += '$'

		self.regex = re.compile(pattern)

		assert len(self.regex.groupindex) in (0, self.regex.groups), \
			('groups in url regexes must either be all named or all positional: %r' % self.regex.pattern)

		self.handler_class = handler_class
		self.kwargs = kwargs or {}
		self.name   = name

		self._path, self._group_count = self._find_groups()

	def __repr__(self):
		return '{}({!r}, {}, kwargs={!r}, name={!r})'.format(type(self).__name__, self.regex.pattern,
			self.handler_class, self.kwargs, self.name)

	def _find_groups(self):
		"""Returns a tuple (reverse string, group count) for a url.

		For example: Given the url pattern /([0-9]{4})/([a-z-]+)/, this method
		would return ('/%s/%s/', 2).
		"""

		pattern = self.regex.pattern

		if pattern.startswith('^'):
			pattern = pattern[1:]

		if pattern.endswith('$'):
			pattern = pattern[:-1]

		if self.regex.groups != pattern.count('('):
			# The pattern is too complicated for our simplistic matching,
			# so we can't support reversing it.
			return (None, None)

		pieces = []

		for fragment in pattern.split('('):
			if ')' in fragment:
				paren_loc = fragment.index(')')
				if paren_loc >= 0:
					pieces.append('%s' + fragment[paren_loc + 1:])
			else:
				pieces.append(fragment)

		return (''.join(pieces), self.regex.groups)

	def reverse(self, *args, **kwargs):
		if not self._path:
			raise URLReverseError('Cannot reverse url regex {!r}'.format(self.regex.pattern))

		if len(args) != self._group_count:
			raise URLReverseError('Required number of arguments not found')

		rv = self._path

		if args:
			converted_args = []
			for arg in args:
				if not isinstance(arg, (str, bytes)):
					arg = str(arg)
				converted_args.append(escape.url_escape(escape.utf8(arg)))
			rv = rv % tuple(converted_args)

		if kwargs:
			items = list(kwargs.items())
			items.sort(key=lambda el: el[0])
			rv += '?' + urllib_parse.urlencode(items)

		return rv
