# -*- coding: utf-8 -*-
# Copyright (c) Divmod

import zope.component as _zc
import zope.interface as _zi

import twisted.internet.defer as defer
import twisted.web.resource   as _twres
import twisted.web.server     as _twsrv

from . import inevow


@_zc.adapter(_twres.IResource)
@_zi.implementer(inevow.IResource)
class TwistedResourceAdapter:
	real_prepath_len = None

	def __init__(self, original):
		self.original = original

	def __repr__(self):
		return '<{} @ 0x{:x} adapting {!r}>'.format(type(self).__name__, id(self), self.original)

	def locateChild(self, ctx, segments):
		request = inevow.IRequest(ctx)

		if self.original.isLeaf:
			self.real_prepath_len = len(request.prepath)
			return self, ()

		name = segments[0]
		request.prepath.append(request.postpath.pop(0))
		resource = self.original.getChildWithDefault(name, request)
		request.postpath.insert(0, request.prepath.pop())

		if isinstance(resource, defer.Deferred):
			return resource.addCallback(lambda resource: (resource, segments[1:]))

		return resource, segments[1:]

	def renderHTTP(self, ctx):
		request = inevow.IRequest(ctx)

		if self.real_prepath_len is not None:
			request.postpath = request.prepath + request.postpath
			request.prepath  = request.postpath[:self.real_prepath_len]
			del request.postpath[:self.real_prepath_len]

		result = self.original.render(request)

		if result == _twsrv.NOT_DONE_YET:
			return request.setNotDoneYetResource(self)

		return defer.succeed(result)

	def willHandle_notFound(self, request):
		if hasattr(self.original, 'willHandle_notFound'):
			return self.original.willHandle_notFound(request)
		return False

	def renderHTTP_notFound(self, ctx):
		return self.original.renderHTTP_notFound(ctx)


@_zi.implementer(inevow.IResource)
class RootResource:
	def __init__(self, application):
		self.application = application

	def locateChild(self, ctx, segments):
		path    = '/' + b'/'.join(segments).decode('ascii')
		request = inevow.IRequest(ctx)
#		url = inevow.IRequest(ctx).URLPath()
#		url = url.click(path)

		handler  = None
		args     = []
		kwargs   = {}
		handlers = self.application.getHostHandlers(request)

		for spec in handlers:
			match = spec.regex.match(path)
			if match:
				handler = spec.handler_class(self.application, request, **spec.kwargs)
				return _zc.getMultiAdapter((handler, spec), inevow.IResource)

		if not handler:
			if self.application.error_handler is None:
				return _twres.NoResource()
			handler = self.application.error_handler(self, request, status_code=404)

		if not handler:
			return _twres.NoResource()

		return inevow.IResource(handler)

	def renderHTTP(self, ctx):
		pass
