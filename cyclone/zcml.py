# -*- coding: utf-8 -*-

import os
import importlib

import zope.component.interface     as _zci
import zope.configuration.xmlconfig as xmlconfig
import zope.interface.interfaces    as _zii

import twisted.logger

from . import types


logger = twisted.logger.Logger()


def loadFallbackIfRequired():
	if not _zci.queryInterface(types.ICyclone.__identifier__):
		package_name = __package__.split('.', 1)[0]
		loadZCML(package_name)


def loadZCML(zcml, debug=None):
	def include_zcml(ctx, zcml):
		if '\n' not in zcml and os.path.isfile(zcml):
			logger.debug('Include ZCML: {filename}', filename=zcml)
			return xmlconfig.file(zcml, context=ctx, execute=False)

		if '\n' not in zcml and ':' not in zcml:
			try:
				importlib.import_module(zcml)
			except ImportError:
				pass
			else:
				zcml += ':configure.zcml'

		if '\n' not in zcml and zcml.count(':') == 1:
			package, zcmlpath = zcml.split(':')
			package = importlib.import_module(package)
			logger.debug('Include ZCML: {filename} ({package})', filename=zcmlpath, package=package.__name__)
			return xmlconfig.file(zcmlpath, package, context=ctx, execute=False)

		logger.debug('Include ZCML string')

		return xmlconfig.string(zcml, context=ctx, execute=False)

	zcml_ctx = xmlconfig.ConfigurationMachine()
	xmlconfig.registerCommonDirectives(zcml_ctx)

	if debug is True:  # or debug is None and settings.run.debug:
		zcml_ctx.provideFeature('devmode')

#	with log.disable('config'):
	if type(zcml) in (tuple, list):
		for item in zcml:
			zcml_ctx = include_zcml(zcml_ctx, item)
	else:
		zcml_ctx = include_zcml(zcml_ctx, zcml)

	zcml_ctx.execute_actions(clear=True)
